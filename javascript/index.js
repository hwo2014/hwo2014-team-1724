var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];

console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

var carCommand = (function() {
  function send(json) {
    client.write(JSON.stringify(json));
    return client.write('\n');
  }

  return {
    join: function() {
      return send({
        msgType: "join",
        data: {
          name: botName,
          key: botKey
        }
      });
    },
    ping: function() {
      return send({
        msgType: "ping",
        data: {}
      });
    },
    switchLane: function(direction) {
      if (typeof direction !== "string") {
        throw new TypeError("switchlane direction must be a string");
      }

      return send({
        msgType: "switchLane",
        data: direction
      });
    },
    throttle: function(amount) {
      if (typeof amount !== "number") {
        throw new TypeError("throttle amount must be a number");
      }

      console.log('Setting throttle to ' + amount);

      return send({
        msgType: "throttle",
        data: amount
      });
    }
  };
})();

var track = null;

var handleThrottle = function(carPosition) {
  console.log(carPosition[0].piecePosition);
  console.log(track[carPosition[0].piecePosition.pieceIndex]);
  console.log(carPosition[0].angle);

  if (carPosition[0].angle === 0) {
    carCommand.throttle(1);
  } else {
    carCommand.throttle(0.5);
  }
};

var responseManager = function(data) {
  // Message sequence:
  // <JOIN
  // >YOURCAR
  // >GAMEINIT
  // >GAMESTART
  // until >GAMEEND
  //  >CARPOSITIONS
  //  <THROTTLE
  // >GAMEEND
  // >TOURNAMENTEND

  switch (data.msgType) {
    case 'yourCar':
      console.log('Registered to race as ' + data.data.name);
      break;
    case 'gameInit':
      // build track?
      // set race vars
      track = data.data.race.track.pieces;
      console.log('Race initialized on track ' + data.data.race.track.name);
      break;
    case 'gameStart':
      console.log('Race started');
      break;
    case 'carPositions':
      handleThrottle(data.data);
      break;
    case 'gameEnd':
      console.log('Race finished');
      break;
    case 'tournamentEnd':
      break;

    // edge cases
    case 'crash':
      console.log('CRASH by ' + data.data.name);
    case 'spawn':
      console.log('Spawn by ' + data.data.name);
    case 'turboAvailable':
      console.log('Turbo ' + data.data.turboFactor  + ' available for ' + data.data.turboDurationTicks + ' ticks');
    case 'lapFinished':
      console.log('Lap finished by ' + data.data.car.name);
    case 'finish':
      console.log('Finish by ' + data.data.car.name);
    case 'dnf':
      console.log('Did not finish by ' + data.data.car.name);
    default:
      carCommand.ping();
  }
};

var errorManager = function() {
  return console.log("disconnected");
};

var client = net.connect(serverPort, serverHost, carCommand.join);
jsonStream = client.pipe(JSONStream.parse());
jsonStream.on('data', responseManager);
jsonStream.on('error', errorManager);
